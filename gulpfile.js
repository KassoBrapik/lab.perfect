/**
 * Created by Kasso on 14.08.2017.
 */
var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();

// sass
gulp.task('sass', function () {
    return gulp.src('./app/sass/main.sass')
        .pipe(sass({outputStyle: 'expanded'})).on('error', sass.logError)
        .pipe(autoprefixer())
        .pipe(gulp.dest('./app/css'))
        .pipe(browserSync.stream())
});

// reload
gulp.task('browser-sync', function() {
    browserSync.init({
        proxy: "perfect"
    });
});

// watch
gulp.task('watch', function () {
    gulp.watch('./app/**/*.sass', ['sass']);
    gulp.watch(["./app/**/*.php", "./app/**/*.html", "./app/**/*.js"]).on('change', browserSync.reload);
});

gulp.task('default', ['sass', 'browser-sync', 'watch']);
