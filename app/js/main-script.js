//Owl-carousel
//////////////////////////
$(document).ready(function(){
    $('.owl-carousel').owlCarousel({
        items: 3,
        loop: false,
        nav: true,
        //Delete the content of Default nav tags
        navText: [
            '<div class="prev"></div>',
            '<div class="next"></div>'
        ]
    });

//SlideDown for Account Form
////////////////////////////
    $('.header-account-btn').click(function(){

        if (!$(this).next().is(":visible")) {
            $(this).next().slideDown('fast');
        }
        else {
            $(this).next().slideUp('fast');
        }
        //Delete the default link properties
        return false;
    });

//Callback icon and form
//////////////////////////
///Animation for #callback
//////////////////////////
    $(window).scroll(function() {
        //Fuction Scroll offset visible
        if($(this).scrollTop() != 0) {
            //Function TimeOUT
            setTimeout(function()
            {
                $('#callback').fadeIn();

            }, 15000);

        } else {

            $('#callback').fadeOut();

        };

    });

    $('#callback').click(function (e) {
        e.preventDefault();
        $(this).toggleClass('active').next().slideToggle();
        // $(this).slideUp(2000);
        // $('#callback-click').show();
        // $('#callback_form').show("slow");
        if($(this).is("animated tada")){
            $(this).addClass("animated tada");
        };
    });

//Scroll effect for Up Button
/////////////////////////////
    $(function() {
        $(window).scroll(function() {
            if($(this).scrollTop() != 0) {
                $('#toTop').fadeIn();
            } else {
                $('#toTop').fadeOut();
            }
        });
        //Return to begin of screen
        $('#toTop').click(function() {
            $('body,html').animate({scrollTop:0},800);
        });
    });

//     $("body").css("display", "none"); /** body (здесь и далее) означает, что эффект применяется ко всей странице. Можно изменить на идентификаторы любых элементов (#content, .comments и т.д.) */
//     $("body").fadeIn(600); /** время появления в миллисекундах */
//     $("a.fade").click(function(event){ /** a.fade означает, что данное решение будет работать только при нажатии на ссылки с классом (class) "fade" (можно изменить на свой) */
//         event.preventDefault();
//         linkLocation = this.href;
//         $("body").fadeOut(600, redirectPage); /** время изчезания в миллисекундах */
//     });
//     function redirectPage() {
//         window.location = linkLocation;
//     }


////////////////////////////////////
    $('.angle-down').click(function(){

        if (!$(this).next().is(":visible")) {
            $(this).next().slideDown('fast');
        }
        else {
            $(this).next().slideUp('fast');
        };
        $(this).toggleClass("fa-angle-up fa-angle-down");
    });



    $(".fourth-column .quantity-plus").on("click", function(){

        $(".fourth-column .quantity").val(parseInt($(".fourth-column .quantity").val()) + 1);
    })

    $(".fourth-column .quantity-minus").on("click", function(){
        if(parseInt($(".fourth-column .quantity").val()) > 1) {
            $(".fourth-column .quantity").val(parseInt($(".fourth-column .quantity").val()) - 1);
        }

    })


});
